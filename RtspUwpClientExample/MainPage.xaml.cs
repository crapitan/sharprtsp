﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Rtsp;
using Rtsp.Messages;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Media.Streaming.Adaptive;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace RtspUwpClientExample
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        private string username, password, hostname, realm, nonce, session, video_codec, audio_codec;
        private int frame = 0, port, cseq = 1, video_payload = -1, video_data_channel = -1, video_rtcp_channel; 

        string url = "rtsp://root:atlas@192.168.100.50:554/axis-media/media.amp?videocodec=h264";
        DateTime firstFrame = DateTime.Now;
        string framesSeconds = "N/A";

        private RTSPClient rtspClient;

        H264RtpMediaStreamSource streamSource = new H264RtpMediaStreamSource();

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.streamSource.StreamSource.Starting += StreamSource_Starting;
        }

        private void StreamSource_Starting(MediaStreamSource sender, MediaStreamSourceStartingEventArgs args)
        {
            this.log.Text = $"{args.Request.StartPosition} starting";
        }

        private RtspTcpTransport rtspTcpTransport;
        private RtspListener tcpListner;
        private RtpPacket pck;
        private RtpFrame frm;

        public MainPage()
        {
            this.InitializeComponent();

            var uri = new Uri(this.url);
            hostname = uri.Host;
            port = uri.Port;

            if (uri.UserInfo.Length > 0)
            {
                username = uri.UserInfo.Split(new char[] { ':' })[0];
                password = uri.UserInfo.Split(new char[] { ':' })[1];
                this.url = uri.GetComponents((UriComponents.AbsoluteUri & ~UriComponents.UserInfo), UriFormat.UriEscaped);
            }

            rtspClient = new RTSPClient();
            rtspClient.Connect(url, RTSPClient.RTP_TRANSPORT.TCP);
            rtspClient.Received_NALs += RtspClient_Received_NALs;
            rtspClient.Received_SPS_PPS += RtspClient_Received_SPS_PPS;
            rtspClient.Play();

            //this.rtspTcpTransport = new Rtsp.RtspTcpTransport(hostname, port);
            //this.tcpListner = new RtspListener(rtspTcpTransport);
            //this.tcpListner.DataReceived += OnDataReceived;
            //this.tcpListner.MessageReceived += this.MessageReceived;

            frm = new RtpFrame();
        }

        private void RtspClient_Received_SPS_PPS(byte[] sps, byte[] pps)
        {
            // What to do with Sps pps  
        }

        private void RtspClient_Received_NALs(List<byte[]> nal_units)
        {
            try
            {
                foreach (var nalUnit in nal_units)
                {
                    pck = new RtpPacket(nalUnit);
                    Debug.WriteLine($"{pck.SequenceNumber} Package type {pck.Type}");
                    frm.AddPacket(pck);
                }
           
                streamSource.EnqueueFrame(frm);
                frm = new RtpFrame();

            }
            catch (Exception)
            {

                throw;
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Lets start the listner
            //tcpListner.Start();

            //// Start By options
            //RtspRequest options_message = new RtspRequestOptions { RtspUri = new Uri(this.url), CSeq = cseq };
            //tcpListner.SendMessage(options_message);
                       
            base.OnNavigatedTo(e);
        }

        private void OnDataReceived(object sender, RtspChunkEventArgs e)
        {
            pck = new RtpPacket(e.Message.Data);

            // Add to current frame
            frm.AddPacket(pck);

            Debug.WriteLine($"{pck.SequenceNumber} Package type {pck.Type}, Size {pck.Payload.Length} ");

            if (pck.Type != 35)
            {
                return;
            }
            
            streamSource.EnqueueFrame(frm);
            frm = new RtpFrame();
        }
        
        private void LoadCamera1()
        {
            var client = new RTSPClient();
            client.Connect(url, RTSPClient.RTP_TRANSPORT.TCP);
            client.Received_NALs += this.OnFrameRecived;
        }
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.player1.SetMediaStreamSource(streamSource.StreamSource);
            this.player1.RealTimePlayback = true;
            this.player1.Play();
        }

        private void MessageReceived(object sender, Rtsp.RtspChunkEventArgs e)
        {
            // Extract the message
            var message = e.Message as RtspResponse;

            Debug.WriteLine($"{message.CSeq} {message.OriginalRequest}");

            // Check if the Message has an Authenticate header. If so we update the 'realm' and 'nonce'
            this.AuthHeader(message);

            if (message.OriginalRequest == null)
            {
                Debug.WriteLine($"Error {message?.ReturnMessage}");
            }
                        
            // If we get a reply to OPTIONS and CSEQ is 1 (which was our first command), then send the DESCRIBE
            // If we fer a reply to OPTIONS and CSEQ is not 1, it must have been a keepalive command
            if (message.OriginalRequest is RtspRequestOptions)
            {
                // send the DESCRIBE. First time around we have no WWW-Authorise
                cseq++;

                var describe_message = new RtspRequestDescribe { RtspUri = new Uri(url), CSeq = cseq };
                this.tcpListner.SendMessage(describe_message);
            }

            // If we get a reply to DESCRIBE (which was our second command), then prosess SDP and send the SETUP
            if (message.OriginalRequest is RtspRequestDescribe)
            {
                // Got a reply for DESCRIBE

                // First time we send DESCRIBE we will not have the authorization Nonce so we
                // handle the Unauthorized 401 error here and send a new DESCRIBE message

                // Authentication
                if (message.IsOk == false)
                {
                    Debug.WriteLine($"Got Error in DESCRIBE Reply {message.ReturnCode} {message.ReturnMessage}");

                    if (message.ReturnCode == 401 && (message.OriginalRequest.Headers.ContainsKey(RtspHeaderNames.Authorization) == false))
                    {
                        // Error 401 - Unauthorized, but the request did not use Authorizarion.
                        if (username == null || password == null)
                        {
                            // we do nothave a username or password. Abort
                            return;
                        }

                        // Send a new DESCRIBE with authorization
                        string digest_authorization = GenerateDigestAuthorization(username, password, realm, nonce, url, "DESCRIBE");

                        var describe_message = new RtspRequestDescribe { RtspUri = new Uri(url) };

                        if (digest_authorization != null) describe_message.Headers.Add(RtspHeaderNames.Authorization, digest_authorization);

                        this.tcpListner.SendMessage(describe_message);

                        return;
                    }
                    else if (message.ReturnCode == 401 && (message.OriginalRequest.Headers.ContainsKey(RtspHeaderNames.Authorization) == true))
                    {
                        // Authorization failed
                        return;
                    }
                    else
                    {
                        // some other error
                        return;
                    }
                }

                // Examine the SDP
                Debug.Write(Encoding.UTF8.GetString(message.Data));

                Rtsp.Sdp.SdpFile sdp_data;

                using (StreamReader streamReader = new StreamReader(new MemoryStream(message.Data)))
                {
                    sdp_data = Rtsp.Sdp.SdpFile.Read(streamReader);
                }

                // Process each 'Media' Attribute in the SDP (each sub-stream)

                for (int x = 0; x < sdp_data.Medias.Count; x++)
                {
                    bool video = (sdp_data.Medias[x].MediaType == Rtsp.Sdp.Media.MediaTypes.video);

                    if (video && video_payload != -1) continue; // have already matched an video payload
                    
                    if (video)
                    {
                        // search the attributes for control, rtpmap and fmtp
                        // (fmtp only applies to video)
                        string control = "";  // the "track" or "stream id"
                        Rtsp.Sdp.AttributFmtp fmtp = null; // holds SPS and PPS in base64 (h264 video)

                        foreach (Rtsp.Sdp.Attribut attrib in sdp_data.Medias[x].Attributs)
                        {
                            if (attrib.Key.Equals("control"))
                            {
                                string sdp_control = attrib.Value;

                                if (sdp_control.ToLower().StartsWith("rtsp://"))
                                {
                                    control = sdp_control; //absolute path
                                }
                                else
                                {
                                    control = url + "/" + sdp_control; // relative path
                                }
                            }
                            if (attrib.Key.Equals("fmtp"))
                            {
                                fmtp = attrib as Rtsp.Sdp.AttributFmtp;
                            }
                            if (attrib.Key.Equals("rtpmap"))
                            {
                                Rtsp.Sdp.AttributRtpMap rtpmap = attrib as Rtsp.Sdp.AttributRtpMap;

                                // Check if the Codec Used (EncodingName) is one we support
                                string[] valid_video_codecs = { "H264" };
                               
                                if (video && Array.IndexOf(valid_video_codecs, rtpmap.EncodingName) >= 0)
                                {
                                    // found a valid codec
                                    video_codec = rtpmap.EncodingName;
                                    video_payload = sdp_data.Medias[x].PayloadType;
                                }
                            }
                        }

                        // If the rtpmap contains H264 then split the fmtp to get the sprop-parameter-sets which hold the SPS and PPS in base64
                        if (video_codec.Contains("H264") && fmtp != null)
                        {
                            var h264Paramerter = Rtsp.Sdp.H264Parameters.Parse(fmtp.FormatParameter);
                            var sps_pps = h264Paramerter.SpropParameterSets;
                            if (sps_pps.Count() >= 2)
                            {
                                byte[] sps = sps_pps[0];
                                byte[] pps = sps_pps[1];
                            }
                        }

                        // Send the SETUP RTSP command if we have a matching Payload Decoder
                        if (video_payload == -1) continue;
                        
                        RtspTransport transport = null;
                        int data_channel = 0;
                        int rtcp_channel = 0;
                                             
                        // Server interleaves the RTP packets over the RTSP connection
                        // Example for TCP mode (RTP over RTSP)   Transport: RTP/AVP/TCP;interleaved=0-1
                        if (video)
                        {
                            video_data_channel = 0;
                            video_rtcp_channel = 1;
                            data_channel = video_data_channel;
                            rtcp_channel = video_rtcp_channel;
                        }
                        
                        transport = new RtspTransport() { LowerTransport = RtspTransport.LowerTransportType.TCP, Interleaved = new PortCouple(data_channel, rtcp_channel), /* Eg Channel 0 for video. Channel 1 for RTCP status reports */  };
                     
                        // Add authorization (if there is a username and password)
                        string digest_authorization = GenerateDigestAuthorization(username, password, realm, nonce, url, "SETUP");

                        // Send SETUP
                        var setup_message = new RtspRequestSetup { RtspUri = new Uri(control) };
                        setup_message.AddTransport(transport);

                        if (digest_authorization != null) setup_message.Headers.Add("Authorization", digest_authorization);

                        this.tcpListner.SendMessage(setup_message);
                    }
                }
            }

            // If we get a reply to SETUP (which was our third command), then process and then send PLAY
            if (message.OriginalRequest is RtspRequestSetup)
            {
                // Got Reply to SETUP
                if (message.IsOk == false)
                {
                    Debug.WriteLine($"Got Error in SETUP Reply {message.ReturnCode} {message.ReturnMessage}");
                    return;
                }

                Debug.WriteLine($"Got reply from Setup. Session is {message.Session}");

                this.session = message.Session; // Session value used with Play, Pause, Teardown

                // Check the Transport header
                if (message.Headers.ContainsKey(RtspHeaderNames.Transport))
                {
                    RtspTransport transport = RtspTransport.Parse(message.Headers[RtspHeaderNames.Transport]);
                }

                // Send PLAY
                var play_message = new RtspRequestPlay();
                play_message.RtspUri = new Uri(url);
                play_message.Session = session;
                tcpListner.SendMessage(play_message);
            }

            // If we get a reply to PLAY (which was our fourth command), then we should have video being received
            if (message.OriginalRequest is RtspRequestPlay)
            {
                // Got Reply to PLAY
                if (message.IsOk == false)
                {
                    Debug.WriteLine("Got Error in PLAY Reply " + message.ReturnCode + " " + message.ReturnMessage);
                    return;
                }

                Debug.WriteLine("Got reply from Play  " + message.Command);
            }
        }

        private void AuthHeader(RtspResponse message)
        {
            if (message.Headers.ContainsKey(RtspHeaderNames.WWWAuthenticate))
            {
                string www_authenticate = message.Headers[RtspHeaderNames.WWWAuthenticate];

                // Parse www_authenticate
                // EG:   Digest realm="AXIS_WS_ACCC8E3A0A8F", nonce="000057c3Y810622bff50b36005eb5efeae118626a161bf", stale=FALSE
                string[] items = www_authenticate.Split(new char[] { ',', ' ' });

                foreach (string item in items)
                {
                    // Split on the = symbol and load in
                    string[] parts = item.Split(new char[] { '=' });

                    if (parts.Count() >= 2 && parts[0].Trim().Equals("realm"))
                    {
                        realm = parts[1].Trim(new char[] { ' ', '\"' }); // trim space and quotes
                    }
                    else if (parts.Count() >= 2 && parts[0].Trim().Equals("nonce"))
                    {
                        nonce = parts[1].Trim(new char[] { ' ', '\"' }); // trim space and quotes
                    }
                }

                Debug.WriteLine($"WWW Authorize parsed for {realm} {nonce}");
            }
        }

        private async void OnFrameRecived(List<byte[]> nal_units)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                frame++;
                
                if ((DateTime.Now - firstFrame).Seconds > 1)
                {
                    firstFrame = DateTime.Now;
                    framesSeconds = $"{frame} frames/s";
                    frame = 0;
                }

                frameSize.Text = $"{framesSeconds}";

                RtpPacket pck = new RtpPacket(nal_units.First());
                RtpFrame frm = new RtpFrame();
                frm.AddPacket(pck);
                streamSource.EnqueueFrame(frm);
            });
        }

        public string GenerateDigestAuthorization(string username, string password, string realm, string nonce, string url, string command)
        {
            if (username == null || username.Length == 0) return null;
            if (password == null || password.Length == 0) return null;
            if (realm == null || realm.Length == 0) return null;
            if (nonce == null || nonce.Length == 0) return null;

            MD5 md5 = MD5.Create();
            string hashA1 = CalculateMD5Hash(md5, username + ":" + realm + ":" + password);
            string hashA2 = CalculateMD5Hash(md5, command + ":" + url);
            string response = CalculateMD5Hash(md5, hashA1 + ":" + nonce + ":" + hashA2);

            const String quote = "\"";
            string digest_authorization = "Digest username=" + quote + username + quote + ", "
                + "realm=" + quote + realm + quote + ", "
                + "nonce=" + quote + nonce + quote + ", "
                + "uri=" + quote + url + quote + ", "
                + "response=" + quote + response + quote;

            return digest_authorization;
        }

        // MD5 (lower case)
        public string CalculateMD5Hash(MD5 md5_session, string input)
        {
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hash = md5_session.ComputeHash(inputBytes);

            StringBuilder output = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                output.Append(hash[i].ToString("x2"));
            }

            return output.ToString();
        }

    }
}
