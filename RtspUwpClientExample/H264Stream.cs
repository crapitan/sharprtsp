﻿using Media.Codecs.Video.H264;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Media.Core;
using Windows.Media.MediaProperties;

namespace RtspUwpClientExample
{

    public class H264RtpMediaStreamSource
    {
        private readonly List<byte[]> _nalUnits = new List<byte[]>();
        private readonly MediaStreamSource _streamSource;
        private readonly BlockingCollection<RtpFrame> _rtpFrameQueue = new BlockingCollection<RtpFrame>();

        private TimeSpan _time;
        private bool[] nalTypes = new bool[256];
        private ulong? _oldTimeStamp;
        private ulong _duration;
        private List<MemoryStream> _nalUnitStreams = new List<MemoryStream>();
        private RtpDePacker _dePacker = new RtpDePacker();
        
        public H264RtpMediaStreamSource()
        {
            var videoEncodingProperties = VideoEncodingProperties.CreateH264();
            videoEncodingProperties.Width = 704;
            videoEncodingProperties.Height = 288;

            var videoStreamDecsriptor = new VideoStreamDescriptor(videoEncodingProperties);
            _streamSource = new MediaStreamSource(videoStreamDecsriptor);

            _streamSource.CanSeek = false;
            _streamSource.BufferTime = TimeSpan.Zero;
            _streamSource.Starting += StreamSourceOnStarting;
            _streamSource.Closed += StreamSourceOnClosed;
            _streamSource.Paused += StreamSourceOnPaused;
            _streamSource.IsLive = true;
            _streamSource.SampleRequested += StreamSourceOnSampleRequested;
            _streamSource.SwitchStreamsRequested += StreamSourceOnSwitchStreamsRequested;
        }

        public MediaStreamSource StreamSource
        {
            get { return _streamSource; }
        }

        public void Stop()
        {
            _streamSource.NotifyError(MediaStreamSourceErrorStatus.Other);
        }

        public void EnqueueFrame(RtpFrame frame)
        {
            _rtpFrameQueue.Add(frame);

            UpdateBufferTime();
        }

        private void UpdateBufferTime()
        {
            _streamSource.BufferTime = TimeSpan.Zero;
        }

        private RtpFrame DequeueFrame()
        {
            var dequeueFrame = _rtpFrameQueue.Take();
            UpdateBufferTime();
            return dequeueFrame;
        }
        
        private void StreamSourceOnStarting(MediaStreamSource sender, MediaStreamSourceStartingEventArgs args)
        {
        }

        private void StreamSourceOnClosed(MediaStreamSource sender, MediaStreamSourceClosedEventArgs args)
        {
        }

        private void StreamSourceOnPaused(MediaStreamSource sender, object args)
        {
        }
        
        private void StreamSourceOnSampleRequested(MediaStreamSource mediaStreamSource, MediaStreamSourceSampleRequestedEventArgs sampelRequestEvebt)
        {
            bool discontinuous = false;
            MediaStreamSourceSampleRequestDeferral deferral = null;

            while (!_nalUnits.Any())
            {
                deferral = sampelRequestEvebt.Request.GetDeferral();

                var frame = DequeueFrame();

                if (!frame.Packets.Any())
                {
                    continue;
                }

                if (_oldTimeStamp.HasValue && frame.Packets.First().TimeStamp > _oldTimeStamp)
                {
                    uint newTimeStamp = frame.Packets.First().TimeStamp;
                    _duration = (ulong)(newTimeStamp - _oldTimeStamp);
                }
                else
                {
                    discontinuous = true;
                    _duration = 90000 / 25;
                }

                _oldTimeStamp = frame.Packets.First().TimeStamp;

                try
                {
                    foreach (var rtpPacket in frame.Packets)
                    {
                        _dePacker.ProcessPacket(rtpPacket.Payload, _nalUnitStreams);
                    }
                }
                catch (Exception)
                {

                }

                var nalUnits = _nalUnitStreams.ToList();
                Debug.WriteLine($"Number of NAL UNITS: {nalUnits.Count}");

                foreach (var memoryStream in nalUnits)
                {
                    _nalUnitStreams.RemoveAt(0);
                    var array = memoryStream.ToArray();
                    // Debug.WriteLine("{0} {1}", nalUnits.Count, new NalUnit(array).Type);
                    _nalUnits.Add(array);
                }

                // var frameNalUnits = DepacketizeFrame(frame);

                // _nalUnits.AddRange(frameNalUnits);
            }

            var nalUnit = _nalUnits.First();
            _nalUnits.RemoveAt(0);

            var unit = new NalUnit(nalUnit);
            //Debug.WriteLine(unit.Type);

            var sample = MediaStreamSample.CreateFromBuffer(nalUnit.AsBuffer(), _time);

            sample.Discontinuous = discontinuous;

            var sampleDuration = A(unit);
            _time += sampleDuration;
            sample.Duration = sampleDuration;
            sample.KeyFrame = unit.Type == 5 || unit.Type == 7 || unit.Type == 8;

            // Set data I guess
            sampelRequestEvebt.Request.Sample = sample;

            if (deferral != null)
            {
                deferral.Complete();
            }
        }
        
        private TimeSpan A(NalUnit nalUnit)
        {
            nalTypes[nalUnit.Type] = true;
            
            if (nalUnit.Type == 5 || nalUnit.Type == 1)
            {
                var d = 1 / 90000.0;
                var sec = d * _duration;
            
                if (_rtpFrameQueue.Count > 0)
                {
                    sec = sec / 10;
                    Debug.WriteLine(_rtpFrameQueue.Count);
                }

                return TimeSpan.FromSeconds(sec);
            }

            return TimeSpan.Zero;
        }

        private static List<byte[]> DepacketizeFrame(RtpFrame frame)
        {
            var nalUnitStreams = new List<MemoryStream>();

            foreach (var rtpPacket in frame.Packets.Where(x => x.Type == 97))
            {
                var depacker = new RtpDePacker();
                depacker.ProcessPacket(rtpPacket.Payload, nalUnitStreams);
            }

            var depacker2 = new RtpDePacker();
            foreach (var rtpPacket in frame.Packets.Where(x => x.Type == 35))
            {
                depacker2.ProcessPacket(rtpPacket.Payload, nalUnitStreams);
            }

            var frameNalUnits = nalUnitStreams
                .Select(nalUnitStream => nalUnitStream.ToArray())
                .ToList();

            return frameNalUnits;
        }

        private void StreamSourceOnSwitchStreamsRequested(MediaStreamSource sender, MediaStreamSourceSwitchStreamsRequestedEventArgs args)
        {
            throw new NotImplementedException();
        }
    }

    public class RtpFrame
    {
        private readonly List<RtpPacket> _packets = new List<RtpPacket>();

        public void AddPacket(RtpPacket packet)
        {
            _packets.Add(packet);
        }

        public IReadOnlyList<RtpPacket> Packets
        {
            get { return _packets; }
        }
    }

    public class NalUnit
    {
        private int _offset;
        private byte[] _nalUnit;

        public NalUnit(byte[] nalUnit)
        {
            _nalUnit = nalUnit;
            _offset = 3;

            if (nalUnit[0] == 0x00 && nalUnit[1] == 0x00 && nalUnit[2] == 0x01)
            {
                _offset = 3;
            }
            else if (nalUnit[0] == 0x00 && nalUnit[1] == 0x00 && nalUnit[2] == 0x00 && nalUnit[3] == 0x01)
            {
                _offset = 4;
            }
            else
            {
                throw new Exception();
            }
        }

        public byte Type
        {
            get { return (byte)(_nalUnit[_offset] & 0x1F); }
        }
    }

    public class RtpPacket
    {
        private byte[] _packet;

        public RtpPacket(byte[] packet)
        {
            _packet = packet;
        }

        public uint TimeStamp
        {
            get { return (uint)((_packet[4] << 24) | (_packet[5] << 16) | (_packet[6] << 8) | (_packet[7])); }
        }

        public ushort SequenceNumber
        {
            get { return (ushort)(_packet[2] << 8 | _packet[3]); }
        }

        public byte CSRCCount
        {
            get { return (byte)(_packet[0] & 0xF); }
        }

        public bool Mark
        {
            get { return (_packet[1] & 0x80) == 0x80; }
        }

        public byte Type
        {
            get { return (byte)(_packet[1] & 0x7F); }
        }

        public bool Padding
        {
            get { return (_packet[0] & 0x20) == 0x20; }
        }

        public byte[] Payload
        {
            get
            {
                var payload = new byte[_packet.Length - 12];
                Array.Copy(_packet, 12, payload, 0, payload.Length);
                return payload;
            }
        }
    }

    public class RtpDePacker
    {
        List<byte> m_ContainedNalTypes = new List<byte>();
        private MemoryStream _fragmentedTempStream;

        /// <summary>
        /// Depacketizes a single packet.
        /// </summary>
        /// <param name="packetData"></param>
        /// <param name="nalUnitList"></param>
        /// <param name="packet"></param>
        /// <param name="containsSps"></param>
        /// <param name="containsPps"></param>
        /// <param name="containsSei"></param>
        /// <param name="containsSlice"></param>
        /// <param name="isIdr"></param>
        public void ProcessPacket(byte[] packetData, List<MemoryStream> nalUnitList)
        {
            //Starting at offset 0
            int offset = 0;

            //Cache the length
            int count = packetData.Length;

            //Must have at least 2 bytes
            if (count <= 2) return;

            //Determine if the forbidden bit is set and the type of nal from the first byte
            byte firstByte = packetData[offset];

            //bool forbiddenZeroBit = ((firstByte & 0x80) >> 7) != 0;

            byte nalUnitType = (byte)(firstByte & Binary.FiveBitMaxValue);

            //TODO

            //o  The F bit MUST be cleared if all F bits of the aggregated NAL units are zero; otherwise, it MUST be set.
            //if (forbiddenZeroBit && nalUnitType <= 23 && nalUnitType > 29) throw new InvalidOperationException("Forbidden Zero Bit is Set.");

            //Optomize setting out parameters, could be done with a label or with a static function.

            //Determine what to do
            switch (nalUnitType)
            {
                //Reserved - Ignore
                case NalUnitType.Unknown:
                case NalUnitType.PayloadContentScalabilityInformation:
                case NalUnitType.Reserved:
                    {
                        //May have 4 byte NAL header.
                        //Do not handle
                        return;
                    }
                case NalUnitType.SingleTimeAggregationA: //STAP - A
                case NalUnitType.SingleTimeAggregationB: //STAP - B
                case NalUnitType.MultiTimeAggregation16: //MTAP - 16
                case NalUnitType.MultiTimeAggregation24: //MTAP - 24
                    {
                        //Move to Nal Data
                        ++offset;

                        //Todo Determine if need to Order by DON first.
                        //EAT DON for ALL BUT STAP - A
                        if (nalUnitType != NalUnitType.SingleTimeAggregationA) offset += 2;

                        //Consume the rest of the data from the packet
                        while (offset < count)
                        {
                            var stream = new MemoryStream();
                            //Determine the nal unit size which does not include the nal header
                            int tmp_nal_size = Binary.Read16(packetData, offset, BitConverter.IsLittleEndian);
                            offset += 2;

                            //If the nal had data then write it
                            if (tmp_nal_size > 0)
                            {

                                //Store the nalType contained
                                m_ContainedNalTypes.Add(nalUnitType);

                                //For DOND and TSOFFSET
                                switch (nalUnitType)
                                {
                                    case NalUnitType.MultiTimeAggregation16:// MTAP - 16
                                        {
                                            //SKIP DOND and TSOFFSET
                                            offset += 3;
                                            goto default;
                                        }
                                    case NalUnitType.MultiTimeAggregation24:// MTAP - 24
                                        {
                                            //SKIP DOND and TSOFFSET
                                            offset += 4;
                                            goto default;
                                        }
                                    default:
                                        {
                                            //Read the nal header but don't move the offset
                                            byte nalHeader = (byte)(packetData[offset] & Binary.FiveBitMaxValue);

                                            //Store the nalType contained
                                            m_ContainedNalTypes.Add(nalHeader);

                                            if (nalHeader == 6 || nalHeader == 7 || nalHeader == 8) stream.WriteByte(0);

                                            //Done reading
                                            break;
                                        }
                                }

                                //Write the start code
                                stream.Write(NalUnitType.StartCode, 0, 3);

                                //Write the nal header and data
                                stream.Write(packetData, offset, tmp_nal_size);

                                nalUnitList.Add(stream);

                                //Move the offset past the nal
                                offset += tmp_nal_size;
                            }
                        }

                        return;
                    }
                case NalUnitType.FragmentationUnitA: //FU - A
                case NalUnitType.FragmentationUnitB: //FU - B
                    {


                        /*
                         Informative note: When an FU-A occurs in interleaved mode, it
                         always follows an FU-B, which sets its DON.
                         * Informative note: If a transmitter wants to encapsulate a single
                          NAL unit per packet and transmit packets out of their decoding
                          order, STAP-B packet type can be used.
                         */
                        //Need 2 bytes
                        if (count > 2)
                        {
                            //Read the Header
                            byte FUHeader = packetData[++offset];

                            bool Start = ((FUHeader & 0x80) >> 7) > 0;

                            bool End = ((FUHeader & 0x40) >> 6) > 0;

                            //bool Receiver = (FUHeader & 0x20) != 0;

                            //if (Receiver) throw new InvalidOperationException("Receiver Bit Set");

                            //Move to data
                            ++offset;

                            //Todo Determine if need to Order by DON first.
                            //DON Present in FU - B
                            if (nalUnitType == 29) offset += 2;

                            //Determine the fragment size
                            int fragment_size = count - offset;

                            //If the size was valid
                            if (fragment_size > 0)
                            {

                                //If the start bit was set
                                if (Start)
                                {
                                    _fragmentedTempStream = new MemoryStream();
                                    //Reconstruct the nal header
                                    //Use the first 3 bits of the first byte and last 5 bites of the FU Header
                                    byte nalHeader = (byte)((firstByte & 0xE0) | (FUHeader & Binary.FiveBitMaxValue));

                                    //Store the nalType contained
                                    m_ContainedNalTypes.Add(nalHeader);

                                    if (nalHeader == 6 || nalHeader == 7 || nalHeader == 8) _fragmentedTempStream.WriteByte(0);

                                    //Write the start code
                                    _fragmentedTempStream.Write(NalUnitType.StartCode, 0, 3);

                                    //Write the re-construced header
                                    _fragmentedTempStream.WriteByte(nalHeader);
                                }


                                //Write the data of the fragment.
                                _fragmentedTempStream.Write(packetData, offset, fragment_size);

                                //Allow If End to Write End Sequence?
                                if (End)
                                {
                                    nalUnitList.Add(_fragmentedTempStream);
                                }

                            }
                        }

                        return;
                    }
                default:
                    {
                        var stream = new MemoryStream();
                        //Store the nalType contained
                        m_ContainedNalTypes.Add(nalUnitType);

                        if (nalUnitType == 6 || nalUnitType == 7 || nalUnitType == 8)
                        {
                            stream.WriteByte(0);
                        }

                        // Write the start code
                        stream.Write(NalUnitType.StartCode, 0, 3);

                        // Write the nal heaer and data data
                        stream.Write(packetData, offset, count - offset);

                        nalUnitList.Add(stream);
                        return;
                    }
            }
        }
    }
}

namespace Media.Codecs.Video.H264
{
    public static class NalUnitType
    {
        public static byte[] StartCode = new byte[] { 0x00, 0x00, 0x01 };

        public const byte Unknown = 0;

        public const byte CodedSlice = 1;

        public const byte DataPartitionA = 2;

        public const byte DataPartitionB = 3;

        public const byte DataPartitionC = 4;

        public const byte InstantaneousDecoderRefresh = 5;

        public const byte SupplementalEncoderInformation = 6;

        public const byte SequenceParameterSet = 7;

        public const byte PictureParameterSet = 8;

        public const byte AccessUnitDelimiter = 9;

        public const byte EndOfSequence = 10;

        public const byte EndOfStream = 11;

        public const byte FillerData = 12;

        public const byte SequenceParameterSetExtension = 13;

        public const byte Prefix = 14;

        public const byte SequenceParameterSetSubset = 15;

        public const byte AuxiliarySlice = 19;

        public const byte SliceExtension = 20;

        public const byte SingleTimeAggregationA = 24;

        public const byte SingleTimeAggregationB = 25;

        public const byte MultiTimeAggregation16 = 26;

        public const byte MultiTimeAggregation24 = 27;

        public const byte FragmentationUnitA = 28;

        public const byte FragmentationUnitB = 29;

        public const byte PayloadContentScalabilityInformation = 30;

        public const byte Reserved = 31;
        public static bool IsReserved(byte type) { return type >= 16 && type <= 18 || type >= 22 && type <= 23 || type == Reserved; }

        public const byte NonInterleavedMultiTimeAggregation = Reserved;
    }

    #region Binary class

    /// <summary>
    /// Provides methods which are useful when working with binary data
    /// </summary>
    [CLSCompliant(true)]
    public static class Binary
    {
        #region Constants

        /// <summary>
        /// 0
        /// </summary>
        internal const int Nihil = 0;

        /// <summary>
        /// 1
        /// </summary>
        internal const int Ūnus = 1;

        /// <summary>
        /// 2
        /// </summary>
        internal const int Duo = 2;

        /// <summary>
        /// 3
        /// </summary>
        internal const int Tres = 3;

        /// <summary>
        /// 4
        /// </summary>
        internal const int Quattuor = 4;

        /// <summary>
        /// 5
        /// </summary>
        internal const int Quinque = 5;

        /// <summary>
        /// 6
        /// </summary>
        internal const int Sex = 6;

        /// <summary>
        /// 7
        /// </summary>
        internal const int Septem = 7;

        /// <summary>
        /// 8
        /// </summary>
        internal const int Octo = 8;

        /// <summary>
        /// 9
        /// </summary>
        internal const int Novem = 9;

        /// <summary>
        /// 10
        /// </summary>
        internal const int Decem = 10;

        /// <summary>
        /// 15
        /// </summary>
        internal const int Quīndecim = 15;

        /// <summary>
        /// 16
        /// </summary>
        internal const int Sēdecim = 16;

        /// <summary>
        /// 31
        /// </summary>
        internal const int TrīgintāŪnus = 31;

        /// <summary>
        /// 42
        /// </summary>
        internal const uint QuadrāgintāDuo = 42;

        #endregion
        
        /// <summary>
        /// Converts the given amount of bytes to the amount of bits needed to represent the same data using the specified options.
        /// </summary>
        /// <param name="byteCount"></param>
        /// <param name="bitSize"></param>
        /// <returns></returns>
        [CLSCompliant(false)]
        public static uint BytesToBits(ref uint byteCount, uint bitSize = Binary.BitsPerByte)
        {
            return byteCount * bitSize;
        }

        public const int BitsPerByte = Binary.Octo;
        public const int BytesPerShort = sizeof(short);
        public const byte FiveBitMaxValue = Binary.TrīgintāŪnus;
        
        /// <summary>
        /// Calculates a 64 bit value from the given parameters.
        /// Throws an <see cref="ArgumentException"/> if <paramref name="sizeInBytes"/> is less than or equal to 0.
        /// </summary>
        /// <param name="octets">The sequence of <see cref="Byte"/> to enumerate</param>
        /// <param name="offset">The offset to skip to in the enumeration</param>
        /// <param name="sizeInBytes">The size of the binary representation of the integer to calculate</param>
        /// <param name="reverse">If true the sequence will be reversed before being calculated</param>
        /// <param name="sign">The value to use as a sign</param>
        /// <param name="shift">The amount of bits to shift the sign for each byte</param>
        /// <returns>The calculated result</returns>
        public static long ReadInteger(IEnumerable<byte> octets, int offset, int sizeInBytes, bool reverse,
            long sign = Binary.Ūnus, int shift = Binary.BitsPerByte)
        {

            if (sizeInBytes < Binary.Nihil) throw new ArgumentException("sizeInBytes", "Must be at greater than 0.");

            if (sizeInBytes > Binary.Octo)
                throw new NotSupportedException("Only sizes up to 8 octets are supported in a long.");

            if (sizeInBytes == 0 || sign == 0) return Binary.Nihil;

            unchecked
            {
                //Start with 0
                long value = Binary.Nihil;

                //Select the range
                var selected = octets.Skip(offset).Take(sizeInBytes);

                //Reverse it
                if (reverse) selected = selected.Reverse();

                //Using an enumerator
                using (var enumerator = selected.GetEnumerator())
                {
                    //While there is a value to read
                    while (enumerator.MoveNext())
                    {
                        //If the byte is greater than 0
                        if (enumerator.Current > byte.MinValue)
                        {
                            //Combine the result of the calculation of the calulated value with the binary representation.
                            value |= (uint)enumerator.Current * sign;
                        }

                        //Move the sign shift left
                        sign <<= shift;
                    }
                }

                return value;
            }
        }
        

        /// <summary>
        /// Reads an unsigned 16 bit value type from the given buffer.
        /// </summary>
        /// <param name="buffer">The buffer to Read the unsigned 16 bit value from</param>
        /// <param name="index">The index in the buffer to Read the value from</param>
        /// <param name="reverse">A value which indicates if the value should be reversed</param>
        /// <returns>The unsigned 16 bit value from the given buffer</returns>
        /// <summary>
        /// The <paramref name="reverse"/> is typically utilized when creating Big ByteOrder \ Network Byte Order or encrypted values.
        /// </summary>
        [CLSCompliant(false)]
        public static ushort ReadU16(IEnumerable<byte> buffer, int index, bool reverse)
        {
            return (ushort)Binary.ReadInteger(buffer, index, Binary.BytesPerShort, reverse);
        }

        public static short Read16(IEnumerable<byte> buffer, int index, bool reverse)
        {
            return (short)Binary.ReadInteger(buffer, index, Binary.BytesPerShort, reverse);
        }
        
        public static byte[] GetBytes(short i, bool reverse = false)
        {
            byte[] result = new byte[Binary.BytesPerShort];
            Write16(result, 0, reverse, i);
            return result;
        }
        
        [CLSCompliant(false)]
        public static void WriteInteger(byte[] buffer, int index, int count, ulong value, bool reverse)
        {
            if (reverse) WriteReversedInteger(buffer, index, count, value);
            else WriteInteger(buffer, index, count, (ulong)value);
        }

        public static void WriteInteger(byte[] buffer, int index, int count, long value, bool reverse)
        {
            WriteInteger(buffer, index, count, (ulong)value, reverse);
        }

        [CLSCompliant(false)]
        public static void WriteInteger(byte[] buffer, int index, int count, ulong value, int shift = Binary.BitsPerByte)
        {
            if (buffer == null || count == 0) return;

            unchecked
            {
                //While something remains
                while (count-- > 0)
                {

                    //Write the byte at the index
                    buffer[index++] = (byte)(value);

                    //Remove the bits we used
                    value >>= shift;
                }

                //For unaligned writes a new function should be created
                //if (value > byte.MinValue && value <= byte.MaxValue) buffer[index - 1] = (byte)value;
            }
        }

        public static void WriteReversedInteger(byte[] buffer, int index, int count, long value)
        {
            WriteReversedInteger(buffer, index, count, value);
        }

        [CLSCompliant(false)]
        public static void WriteReversedInteger(byte[] buffer, int index, int count, ulong value,
            int shift = Binary.BitsPerByte)
        {
            if (buffer == null || count == 0) return;

            unchecked
            {
                //While something remains
                while (count /*--*/> 0)
                {
                    //Write the byte at the reversed index
                    buffer[index + --count] = (byte)(value);

                    //Remove the bits we used
                    value >>= shift;
                }
            }
        }

       
        //Todo
        /// <summary>
        /// Writes a the given unsgined 16 bit value to the buffer at the given index.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <param name="reverse"></param>
        /// <param name="value"></param>
        public static void Write16(byte[] buffer, int index, bool reverse, short value)
        {
            WriteInteger(buffer, index, Binary.BytesPerShort, value, reverse);
        }

        [CLSCompliant(false)]
        public static void Write16(byte[] buffer, int index, bool reverse, ushort value)
        {
            WriteInteger(buffer, index, Binary.BytesPerShort, (short)value, reverse);
        }
        

        #endregion
    }
}
